/* TRESHOLD 1*/
/* Insertion functions */

SELECT add_transport_type('001', 'Bus', 30, 4);
SELECT add_transport_type('002', 'Tram', 100, 5);
SELECT add_transport_type('003', 'Metro', 200, 4);
SELECT add_zone('zone1', 1.0);
SELECT add_zone('zone2', 2.0);
/* Adding metro stations */
SELECT add_station(1, 'Louis Aragon', 'Villejuif', 1, '003');
SELECT add_station(2, 'PVC', 'Villejuif', 1, '003');
SELECT add_station(3, 'Leo Lagrange', 'Villejuif', 1, '003');
/* Adding Tram stations */
SELECT add_station(4, 'Porte d Italie', 'Paris', 1, '002');
SELECT add_station(5, 'Charlety', 'Paris', 1, '002');
SELECT add_station(6, 'Porte de Vincennes', 'Paris', 1, '002');
/* Adding lines */
SELECT add_line('001', '003');
SELECT add_line('002', '002');
/* Adding metro stations */
SELECT add_station_to_line(1, '001', 1);
SELECT add_station_to_line(2, '001', 2);
SELECT add_station_to_line(3, '001', 3);
/* Adding Tram stations */
SELECT add_station_to_line(4, '002', 1);
SELECT add_station_to_line(5, '002', 2);
SELECT add_station_to_line(6, '002', 3);

/* Views */

SELECT * FROM view_transport_50_300_users;
SELECT * FROM view_station_from_villejuif;
SELECT * FROM view_stations_zones;
SELECT * FROM view_nb_station_type;
SELECT * FROM view_line_duration;
SELECT * FROM view_a_station_capacity;

/* Procedures */

SELECT * FROM list_station_in_line('002');

/* TRESHOLD 2 */
/* Insertion functions */

SELECT add_person('Steve', 'Lukather', 'steve.lukather@toto.com', '1234567890',
                  '99, kingdom of desire', 'Villejuif', '12345');
SELECT add_person('Cersei', 'Lannister', 'cersei.lannister@got.com', '1234567890',
                  'erbheirb', 'Kings Landing', '12345');
SELECT add_offer('12345', 'Offer of the year', 50.0, 2, 1, 2);
SELECT add_offer('12346', 'Offer of the year2', 50.0, 2, 1, 2);
SELECT add_offer('12347', 'Offer of the year3', 50.0, 2, 1, 2);
SELECT add_subscription(1, 'steve.lukather@toto.com', '12345', '2014-09-28');
SELECT update_status(1, 'steve.lukather@toto.com', 'Registered');
SELECT add_subscription(2, 'steve.lukather@toto.com', '12346', '2014-09-28');
SELECT add_subscription(3, 'cersei.lannister@got.com', '12345', '2012-09-28');

/* Update Functions */
SELECT update_offer_price('12345', 25.0);

/* Views */
SELECT * FROM view_user_small_name;
SELECT * FROM view_user_subscription;
SELECT * FROM view_unloved_offers;
SELECT * FROM view_pending_subscriptions;
SELECT * FROM view_old_subscription;

/* Procedures */
SELECT list_station_near_user('steve.lukather@toto.com');
SELECT list_subscribers('12345');
SELECT list_subscription('steve.lukather@toto.com', '2010-04-22');

/* TRESHOLD 3 */
/* Insertion Functions */
SELECT add_service('service1', 10);
SELECT add_contract('steve.lukather@toto.com', '2014-06-01', 'service1');
SELECT add_contract('cersei.lannister@got.com', '2014-06-01', 'service1');

/* Update Functions */
SELECT update_service('service1', 100);
SELECT update_employee_email('lukath_s', 's.lukather@toto.com');

/* Views */
SELECT * FROM view_employees;
SELECT * FROM view_nb_employees_per_service;

/* Procedures */
SELECT list_login_employee('2014-07-01');
SELECT worked(1, '2013-06-01');
SELECT * FROM list_not_employee('2014-06-01');

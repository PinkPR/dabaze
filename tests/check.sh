#! /bin/sh

psql dabaze < ../src/init_base.sql
psql dabaze < ../src/level_1.sql
psql dabaze < ../src/level_2.sql
psql dabaze < ../src/level_3.sql
psql dabaze < check.sql

echo
echo "RESULT"
echo

echo TRANSPORT_TYPE
echo "SELECT * FROM transport_type" | psql dabaze

echo ZONE
echo "SELECT * FROM zone" | psql dabaze

echo STATION
echo "SELECT * FROM station" | psql dabaze

echo LINE
echo "SELECT * FROM line" | psql dabaze

echo STATION_LINE
echo "SELECT * FROM station_line" | psql dabaze

echo PERSON
echo "SELECT * FROM person" | psql dabaze

echo OFFER
echo "SELECT * FROM offer" | psql dabaze

echo SUBSCRIPTION
echo "SELECT * FROM subscription" | psql dabaze

echo EMPLOYEE
echo "SELECT * FROM employee" | psql dabaze

echo SERVICE
echo "SELECT * FROM service" | psql dabaze

echo CONTRACT
echo "SELECT * FROM contract" | psql dabaze

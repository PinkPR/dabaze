/*---------------------------------------------------------------------------
|---------------------------------------------------------------------------|
|----------------------------TRESHOLD 3-------------------------------------|
|----------------------------Insertion Functions----------------------------|
|---------------------------------------------------------------------------|
---------------------------------------------------------------------------*/

CREATE OR REPLACE FUNCTION
add_service(name_       VARCHAR(32),
            discount_   INT)
  RETURNS BOOLEAN AS
$$
  BEGIN
    INSERT INTO service(name, discount)
      VALUES(name_, discount_);
    RETURN TRUE;
  EXCEPTION
    WHEN unique_violation THEN
      RETURN FALSE;
    WHEN check_violation THEN
      RETURN FALSE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
add_contract(_mail              VARCHAR(128),
             date_beginning     DATE,
             service_           VARCHAR(32))
  RETURNS BOOLEAN AS
$$
  DECLARE
    firstn  VARCHAR(32);
    lastn   VARCHAR(32);
    num     INT;
    login   VARCHAR(8);
    serv    INT;
  BEGIN
    IF (NOT EXISTS(SELECT * FROM person WHERE email = _mail)) THEN
      RETURN FALSE;
    END IF;
    IF (NOT EXISTS(SELECT * FROM service WHERE name = service_)) THEN
      RETURN FALSE;
    END IF;
    firstn := (SELECT firstname FROM person WHERE email = _mail LIMIT 1 OFFSET 0);
    lastn := (SELECT lastname FROM person WHERE email = _mail LIMIT 1 OFFSET 0);
    num := (SELECT id FROM person WHERE email = _mail LIMIT 1 OFFSET 0);
    serv := (SELECT id FROM service WHERE name = service_ LIMIT 1 OFFSET 0);
    IF (EXISTS(SELECT * FROM employee WHERE employee.person = num)) THEN
      login := (SELECT login FROM employee WHERE employee.person = num LIMIT 1 OFFSET 0);
      INSERT INTO contract(employee, service, start_date)
        VALUES(login, serv, date_beginning);
      RETURN TRUE;
    END IF;
    INSERT INTO employee
      VALUES(CAST(lower(CONCAT(SUBSTRING(lastn from 1 for 6), '_', SUBSTRING(firstn from 1 for 1))) AS VARCHAR(8)),
             num);
    INSERT INTO contract(employee, service, start_date)
      VALUES(CAST(lower(CONCAT(SUBSTRING(lastn from 1 for 6), '_', SUBSTRING(firstn from 1 for 1))) AS VARCHAR(8)),
             serv,
             date_beginning);
    RETURN TRUE;
  EXCEPTION
    WHEN foreign_key_violation THEN
      RETURN FALSE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
end_contract(_mail    VARCHAR(128),
             date_end DATE)
  RETURNS BOOLEAN AS
$$
  DECLARE
    num     INT;
    login_  VARCHAR(8);
  BEGIN
    IF (NOT EXISTS(SELECT * FROM person WHERE email = _mail)) THEN
      RETURN FALSE;
    END IF;
    num := (SELECT id FROM person WHERE person.email = _mail LIMIT 1 OFFSET 0);
    login_ := (SELECT login FROM employee WHERE employee.person = num LIMIT 1 OFFSET 0);
    IF ((SELECT end_date FROM contract WHERE employee = login_ LIMIT 1 OFFSET 0) IS NOT NULL) THEN
      RETURN FALSE;
    END IF;
    UPDATE contract SET end_date = date_end
    WHERE employee = login_ AND end_date IS NULL;
    RETURN TRUE;
  EXCEPTION
    WHEN foreign_key_violation THEN
      RETURN FALSE;
  END;
$$ LANGUAGE plpgsql;

/*---------------------------------------------------------------------------
|---------------------------------------------------------------------------|
|----------------------------TRESHOLD 3-------------------------------------|
|----------------------------Update Functions-------------------------------|
|---------------------------------------------------------------------------|
---------------------------------------------------------------------------*/

CREATE OR REPLACE FUNCTION
update_service(_name        VARCHAR(32),
               _discount    INT)
  RETURNS BOOLEAN AS
$$
  BEGIN
    IF (NOT EXISTS(SELECT * FROM service WHERE name = _name)) THEN
      RETURN FALSE;
    END IF;
    UPDATE service SET discount = _discount WHERE name = _name;
    RETURN TRUE;
  EXCEPTION
    WHEN check_violation THEN
      RETURN FALSE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
update_employee_email(login_    VARCHAR(8),
                      newm      VARCHAR(128))
  RETURNS BOOLEAN AS
$$
  DECLARE
    num   INT;
  BEGIN
    IF (NOT EXISTS(SELECT * FROM employee WHERE login = login_)) THEN
      RETURN FALSE;
    END IF;
    num := (SELECT person FROM employee WHERE login = login_ LIMIT 1 OFFSET 0);
    UPDATE person SET email = newm WHERE id = num;
    RETURN TRUE;
  END;
$$ LANGUAGE plpgsql;

/*---------------------------------------------------------------------------
|---------------------------------------------------------------------------|
|----------------------------TRESHOLD 3-------------------------------------|
|----------------------------Views------------------------------------------|
|---------------------------------------------------------------------------|
---------------------------------------------------------------------------*/

CREATE OR REPLACE VIEW
view_employees(lastname, firstname, login, service)
  AS
  SELECT a, b, c, d FROM (
    SELECT person.lastname as a,
           person.firstname as b,
           employee.login as c,
           service.name as d,
           contract.id
    FROM person, employee, service, contract
    WHERE person.id = employee.person AND contract.employee = employee.login
      AND service.id = contract.service AND contract.end_date IS NULL)
    AS foo
    ORDER BY CONCAT(a, ' ', b);

CREATE OR REPLACE VIEW
view_nb_employees_per_service(service, nb)
  AS
    SELECT service.name, COUNT(contract.id) FROM service, contract
    WHERE service.id = contract.service AND contract.end_date IS NULL
    GROUP BY service.name
    ORDER BY service.name;

/*---------------------------------------------------------------------------
|---------------------------------------------------------------------------|
|----------------------------TRESHOLD 3-------------------------------------|
|----------------------------Procedures-------------------------------------|
|---------------------------------------------------------------------------|
---------------------------------------------------------------------------*/

CREATE OR REPLACE FUNCTION
worked(emp INT, dat DATE)
  RETURNS TEXT AS
$$
  DECLARE
    login_ VARCHAR(8);
  BEGIN
    IF (NOT EXISTS(SELECT * FROM employee WHERE employee.person = emp)) THEN
      RETURN 'NO';
    END IF;
    login_ := (SELECT login FROM employee WHERE employee.person = emp LIMIT 1 OFFSET 0);
    IF EXISTS(SELECT start_date FROM contract WHERE contract.employee = login_ AND contract.start_date <= dat) THEN
      RETURN 'YES';
    END IF;
    RETURN 'NO';
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
list_login_employee(date_service DATE)
  RETURNS SETOF VARCHAR(8) AS
$$
  BEGIN
    RETURN QUERY
    SELECT contract.employee FROM contract
    WHERE contract.start_date <= date_service AND
          (contract.end_date IS NULL OR contract.end_date >= date_service)
    ORDER BY contract.employee;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
list_not_employee(date_service DATE)
  RETURNS TABLE(lastname VARCHAR(32), firstname VARCHAR(32), has_worked TEXT) AS
$$
  BEGIN
    RETURN QUERY
    SELECT person.lastname, person.firstname, worked(employee.person, date_service)
    FROM person, employee
    WHERE person.id = employee.person
    ORDER BY CONCAT(person.lastname, ' ', person.firstname);
  END;
$$ LANGUAGE plpgsql;

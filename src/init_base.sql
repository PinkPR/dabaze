/*==============================================================*/
/* Nom de SGBD :  PostgreSQL 9.2                                */
/*==============================================================*/

DROP TABLE offer_update CASCADE;
DROP TABLE subscription_update CASCADE;
DROP TABLE bill CASCADE;
DROP TABLE journey CASCADE;
DROP TABLE contract CASCADE;
DROP TABLE service CASCADE;
DROP TABLE employee CASCADE;
DROP TABLE subscription CASCADE;
DROP TABLE status CASCADE;
DROP TABLE person CASCADE;
DROP TABLE offer CASCADE;
DROP TABLE line CASCADE;
DROP TABLE station CASCADE;
DROP TABLE station_line CASCADE;
DROP TABLE transport_type CASCADE;
DROP TABLE zone CASCADE;

/*==============================================================*/
/* Threshold 1                                                  */
/*==============================================================*/

CREATE TABLE zone(
    num         SERIAL          PRIMARY KEY,
    name        VARCHAR(32)     NOT NULL,
    price       FLOAT           NOT NULL,
    CHECK       (price > 0.0)
);

CREATE TABLE transport_type(
    code        VARCHAR(3)      PRIMARY KEY,
    name        VARCHAR(32)     NOT NULL,
    capacity    INT             NOT NULL,
    duration    INT             NOT NULL,
    UNIQUE      (code),
    UNIQUE      (name),
    CHECK       (capacity >= 0),
    CHECK       (duration >= 0)
);

CREATE TABLE station(
    id          INT             PRIMARY KEY,
    name        VARCHAR(64)     NOT NULL,
    town        VARCHAR(32)     NOT NULL,
    zone        INT             NOT NULL        REFERENCES zone(num),
    type        VARCHAR(3)      NOT NULL        REFERENCES transport_type(code)
);

CREATE TABLE line(
    code        VARCHAR(3)      PRIMARY KEY,
    type        VARCHAR(3)      NOT NULL        REFERENCES transport_type(code),
    UNIQUE      (code)
);

CREATE TABLE station_line(
    line        VARCHAR(3)      NOT NULL        REFERENCES line(code),
    station     INT             NOT NULL        REFERENCES station(id),
    location    INT             NOT NULL,
    /*UNIQUE      (location),*/
    CHECK       (location >= 0),
    PRIMARY KEY (line, station)
);


/*==============================================================*/
/* Threshold 2                                                  */
/*==============================================================*/

CREATE TABLE offer(
    code        VARCHAR(5)      PRIMARY KEY,
    name        VARCHAR(32)     NOT NULL,
    price       FLOAT           NOT NULL,
    nb_month    INT             NOT NULL,
    zone_start  INT             NOT NULL        REFERENCES zone(num),
    zone_end    INT             NOT NULL        REFERENCES zone(num),
    CHECK       (price > 0)
);

CREATE TABLE person(
    id          SERIAL          PRIMARY KEY,
    firstname   VARCHAR(32)     NOT NULL,
    lastname    VARCHAR(32)     NOT NULL,
    email       VARCHAR(128)    NOT NULL        UNIQUE,
    phone       VARCHAR(10)     NOT NULL,
    address     TEXT            NOT NULL,
    town        VARCHAR(32)     NOT NULL,
    zipcode     VARCHAR(5)      NOT NULL
);

CREATE TABLE status(
    code        VARCHAR(1)      PRIMARY KEY,
    name        VARCHAR(32)     NOT NULL
);
INSERT INTO status VALUES('R', 'Registered');
INSERT INTO status VALUES('P', 'Pending');
INSERT INTO status VALUES('I', 'Incomplete');

CREATE TABLE subscription(
    id          SERIAL          PRIMARY KEY,
    start_date  DATE            NOT NULL,
    status      VARCHAR(1)      NOT NULL        REFERENCES status(code),
    offer       VARCHAR(5)      NOT NULL        REFERENCES offer(code),
    person      INT             NOT NULL        REFERENCES person(id)

);


/*==============================================================*/
/* Threshold 3                                                  */
/*==============================================================*/

CREATE TABLE employee(
    login       VARCHAR(8)      PRIMARY KEY,
    person      INT             NOT NULL        REFERENCES person(id)
);

CREATE TABLE service(
    id          SERIAL          PRIMARY KEY,
    name        VARCHAR(32)     NOT NULL,
    discount    INT             NOT NULL,
    CHECK       (discount >= 0),
    CHECK       (discount <= 100),
    UNIQUE      (name)
);

CREATE TABLE contract(
    id          SERIAL          PRIMARY KEY,
    employee    VARCHAR(8)      NOT NULL        REFERENCES employee(login),
    service     INT             NOT NULL        REFERENCES service(id),
    start_date  DATE            NOT NULL,
    end_date    DATE            NULL
);


/*==============================================================*/
/* Threshold 4                                                  */
/*==============================================================*/

CREATE TABLE journey(
    id          SERIAL          NOT NULL,
    person      INT             NOT NULL        REFERENCES person(id),
    entry_st    INT             NOT NULL        REFERENCES station(id),
    exit_st     INT             NOT NULL        REFERENCES station(id),
    entry_time  TIMESTAMP       NOT NULL,
    exit_time   TIMESTAMP       NOT NULL
);

CREATE TABLE bill(
    num         SERIAL          PRIMARY KEY,
    person      INT             NOT NULL        REFERENCES person(id),
    date_bill   DATE            NOT NULL,
    amount      FLOAT           NOT NULL,
    is_payed    BOOLEAN         NOT NULL
);


/*==============================================================*/
/* Threshold 5                                                  */
/*==============================================================*/

CREATE TABLE offer_update(
    code                VARCHAR(5)              REFERENCES offer(code),
    modif_date          TIMESTAMP,
    old_price           FLOAT8,
    new_price           FLOAT8
);

CREATE TABLE subscription_update(
    email              VARCHAR(128),
    offer              VARCHAR(5),
    modif_date         TIMESTAMP,
    old_status         VARCHAR(32),
    new_status         VARCHAR(32)
);


/*---------------------------------------------------------------------------
|---------------------------------------------------------------------------|
|----------------------------TRESHOLD 1-------------------------------------|
|----------------------------Insertion Functions----------------------------|
|---------------------------------------------------------------------------|
---------------------------------------------------------------------------*/

CREATE OR REPLACE FUNCTION
add_transport_type(code           VARCHAR(3),
                   name           VARCHAR(32),
                   capacity       INT,
                   argv_interval  INT)
  RETURNS BOOLEAN AS
$$
  BEGIN
    INSERT INTO transport_type
      VALUES(code, name, capacity, argv_interval);
    RETURN TRUE;
  EXCEPTION
    WHEN check_violation THEN
      RETURN FALSE;
    WHEN unique_violation THEN
      RETURN FALSE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
add_zone(name     VARCHAR(32),
         price    FLOAT)
  RETURNS BOOLEAN AS
$$
  BEGIN
    INSERT INTO zone (name, price)
      VALUES(name, price);
    RETURN TRUE;
  EXCEPTION
    WHEN check_violation THEN
      RETURN FALSE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
add_station(id      INT,
            name    VARCHAR(64),
            town    VARCHAR(64),
            zone    INT,
            type    VARCHAR(3))
  RETURNS BOOLEAN AS
$$
  BEGIN
    INSERT INTO station
      VALUES(id, name, town, zone, type);
    RETURN TRUE;
  EXCEPTION
    WHEN foreign_key_violation THEN
      RETURN FALSE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
add_line(code     VARCHAR(3),
         type     VARCHAR(3))
  RETURNS BOOLEAN AS
$$
  BEGIN
    INSERT INTO line
      VALUES(code, type);
    RETURN TRUE;
  EXCEPTION
    WHEN unique_violation THEN
      RETURN FALSE;
    WHEN foreign_key_violation THEN
      RETURN FALSE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
add_station_to_line(station     INT,
                    line        VARCHAR(3),
                    pos         INT)
  RETURNS BOOLEAN AS
$$
  BEGIN
    IF EXISTS(SELECT * FROM station_line WHERE station_line.line = $2 AND station_line.location = pos)
      THEN RETURN FALSE; END IF;
    INSERT INTO station_line
      VALUES(line, station, pos);
    RETURN TRUE;
  EXCEPTION
    WHEN foreign_key_violation THEN
      RETURN FALSE;
    /*WHEN unique_violation THEN
      RETURN FALSE;*/
  END;
$$ LANGUAGE plpgsql;

/*---------------------------------------------------------------------------
|---------------------------------------------------------------------------|
|----------------------------TRESHOLD 1-------------------------------------|
|----------------------------Views------------------------------------------|
|---------------------------------------------------------------------------|
---------------------------------------------------------------------------*/

CREATE OR REPLACE VIEW
view_transport_50_300_users(transport)
  AS
    SELECT name FROM transport_type
      WHERE (capacity <= 300) AND (capacity >= 50)
      ORDER BY name;

CREATE OR REPLACE VIEW
view_station_from_villejuif(station)
  AS
    SELECT name FROM station
      WHERE town = 'Villejuif'
      ORDER BY name;

CREATE OR REPLACE VIEW
view_stations_zones(station, zone)
  AS
    SELECT station.name, zone.name FROM station, zone
    WHERE station.zone = zone.num
    ORDER BY zone.num, station.name;

CREATE OR REPLACE VIEW
view_nb_station_type(type, stations)
  AS
    SELECT transport_type.name, COUNT(station.type) FROM transport_type, station
    WHERE transport_type.code = station.type
    GROUP BY station.type, transport_type.name
    ORDER BY COUNT(station.type) DESC, station.type ASC;

CREATE OR REPLACE VIEW
view_line_duration(type, line, minutes)
  AS
    SELECT transport_type.name,
           station_line.line,
           (COUNT(station) - 1) * transport_type.duration
    FROM transport_type, station_line, station
    WHERE transport_type.code = station.type AND station.id = station_line.station
    GROUP BY station_line.line, transport_type.name, transport_type.duration
    ORDER BY transport_type.name, station_line.line;

CREATE OR REPLACE VIEW
view_a_station_capacity(station, capacity)
 AS
  SELECT station.name, transport_type.capacity
    FROM station, transport_type
    WHERE station.type = transport_type.code AND
      (station.name LIKE 'A%' OR station.name LIKE 'a%')
    ORDER BY station.name, transport_type.capacity;

/*---------------------------------------------------------------------------
|---------------------------------------------------------------------------|
|----------------------------TRESHOLD 1-------------------------------------|
|----------------------------Procedures-------------------------------------|
|---------------------------------------------------------------------------|
---------------------------------------------------------------------------*/

CREATE OR REPLACE FUNCTION
list_station_in_line(line_code VARCHAR(3))
  RETURNS SETOF VARCHAR(64) AS
$$
  BEGIN
    RETURN QUERY
    SELECT name FROM (
      SELECT station.name, station_line.station from station, station_line
      WHERE station.id = station_line.station AND station_line.line = line_code
      ORDER BY station_line.location)
      AS foo;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
list_types_in_zone(zoneid INT)
  RETURNS SETOF VARCHAR(32) AS
$$
  BEGIN
    RETURN QUERY
    SELECT name FROM (
      SELECT transport_type.name, station.id FROM transport_type, station
      WHERE transport_type.code = station.type
        AND station.zone = zoneid
      GROUP BY transport_type.name, station.id
      ORDER BY transport_type.name)
      AS foo
      GROUP BY name;
  END;
$$ LANGUAGE plpgsql;

/*CREATE OR REPLACE FUNCTION
get_line_of_station(stat INT)
  RETURNS VARCHAR(3) AS
$$
  BEGIN
    RETURN (SELECT * FROM (
    SELECT station_line.line FROM station_line
    WHERE station_line.station = stat)
    AS foo
    OFFSET 0
    LIMIT 1);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
get_pos_on_line(stat INT)
  RETURNS INT AS
$$
  BEGIN
    RETURN (SELECT * FROM (
      SELECT station_line.location FROM station_line
      WHERE station_line.station = stat)
      AS foo
      OFFSET 0
      LIMIT 1);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
get_stations_between_stations(station_start INT, station_end INT)
  RETURNS SETOF INT AS
$$
  BEGIN
    RETURN QUERY
    SELECT station_line.station FROM station_line
    WHERE station_line.line = get_line_of_station(station_start) AND
      station_line.location >= get_pos_on_line(station_start) AND
      station_line.location <= get_pos_on_line(station_end);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
get_zone_of_station(stat INT)
  RETURNS INT AS
$$
  BEGIN
    RETURN (SELECT * FROM (
      SELECT station.zone FROM station
      WHERE station.id = stat)
      AS foo
      LIMIT 1
      OFFSET 0);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
get_zones_between_stations(station_start INT, station_end INT)
  RETURNS SETOF INT AS
$$
  BEGIN
    RETURN QUERY
    SELECT  FROM get_stations_between_stations(station_start, station_end);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
get_cost_travel(station_start INT, station_end INT)
  RETURNS FLOAT AS
$$
  BEGIN
    IF get_line_of_station(station_start) <> get_line_of_station(station_end) THEN
      RETURN 0.0;
    END IF;
    RETURN
  END;

$$ LANGUAGE plpgsql;*/

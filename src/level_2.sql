/*---------------------------------------------------------------------------
|---------------------------------------------------------------------------|
|----------------------------TRESHOLD 2-------------------------------------|
|----------------------------Insertion Funtions-----------------------------|
|---------------------------------------------------------------------------|
---------------------------------------------------------------------------*/

CREATE OR REPLACE FUNCTION
add_person(firstname    VARCHAR(32),
           lastname     VARCHAR(32),
           email        VARCHAR(128),
           phone        VARCHAR(10),
           address      TEXT,
           town         VARCHAR(32),
           zipcode      VARCHAR(5))
  RETURNS BOOLEAN AS
$$
  BEGIN
    INSERT INTO person (firstname, lastname, email, phone, address, town, zipcode)
      VALUES(firstname, lastname, email, phone, address, town, zipcode);
    RETURN TRUE;
  EXCEPTION
    WHEN unique_violation THEN
      RETURN FALSE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
add_offer(code        VARCHAR(5),
          name        VARCHAR(32),
          price       FLOAT,
          nb_month    INT,
          zone_from   INT,
          zone_to     INT)
  RETURNS BOOLEAN AS
$$
  BEGIN
    INSERT INTO offer
      VALUES(code, name, price, nb_month, zone_from, zone_to);
    RETURN TRUE;
  EXCEPTION
    WHEN foreign_key_violation THEN
      RETURN FALSE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
add_subscription(num        INT,
                 mail       VARCHAR(128),
                 code       VARCHAR(5),
                 date_sub   DATE)
  RETURNS BOOLEAN AS
$$
  DECLARE
    pers INT;
  BEGIN
    IF (NOT EXISTS(SELECT * FROM person WHERE email = mail)) THEN
      RETURN FALSE;
    END IF;
    pers := (SELECT id FROM person WHERE email = mail LIMIT 1 OFFSET 0);
    IF EXISTS(SELECT * FROM subscription WHERE subscription.status <> 'R' AND
              subscription.person = pers) THEN
      RETURN FALSE;
    END IF;
    INSERT INTO subscription (id, start_date, status, offer, person)
      VALUES(num, date_sub, 'P', code, pers);
    RETURN TRUE;
    EXCEPTION
      WHEN foreign_key_violation THEN
        RETURN FALSE;
  END;
$$ LANGUAGE plpgsql;

/*---------------------------------------------------------------------------
|---------------------------------------------------------------------------|
|----------------------------TRESHOLD 2-------------------------------------|
|----------------------------Update Funtions--------------------------------|
|---------------------------------------------------------------------------|
---------------------------------------------------------------------------*/

CREATE OR REPLACE FUNCTION
update_status(num         INT,
              email       VARCHAR(128),
              new_status  VARCHAR(32))
  RETURNS BOOLEAN AS
$$
  BEGIN
    IF (NOT EXISTS(SELECT * FROM subscription WHERE id = num)) THEN
      RETURN FALSE;
    END IF;
    UPDATE subscription
    SET status = (SELECT code FROM status WHERE name = new_status LIMIT 1 OFFSET 0)
    WHERE id = num;
    RETURN TRUE;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
update_offer_price(offer_code   VARCHAR(5),
                   pricee       FLOAT)
  RETURNS BOOLEAN AS
$$
  BEGIN
    IF (NOT EXISTS(SELECT * FROM offer WHERE code = offer_code)) THEN
      RETURN FALSE;
    END IF;
    UPDATE offer
    SET price = pricee
    WHERE code = offer_code;
    RETURN TRUE;
  EXCEPTION
    WHEN foreign_key_violation THEN
      RETURN FALSE;
    WHEN check_violation THEN
      RETURN FALSE;
  END;
$$ LANGUAGE plpgsql;

/*---------------------------------------------------------------------------
|---------------------------------------------------------------------------|
|----------------------------TRESHOLD 2-------------------------------------|
|----------------------------View-------------------------------------------|
|---------------------------------------------------------------------------|
---------------------------------------------------------------------------*/

CREATE OR REPLACE VIEW
view_user_small_name(lastname, firstname)
  AS
    SELECT lastname, firstname FROM person
    WHERE CHAR_LENGTH(lastname) <= 4;

CREATE OR REPLACE VIEW
view_user_subscription(user_, offer)
  AS
    SELECT a, b FROM (
    SELECT CONCAT(person.lastname, ' ', person.firstname) AS a,
           offer.name AS b,
           subscription.id AS c
      FROM person, offer, subscription
    WHERE subscription.person = person.id AND subscription.offer = offer.code)
    AS foo
    ORDER BY a, b;

CREATE OR REPLACE VIEW
view_unloved_offers(offer)
  AS
    SELECT offer.name FROM offer
    LEFT JOIN subscription ON offer.code = subscription.offer
    WHERE subscription.offer IS NULL
    ORDER BY offer.name;

CREATE OR REPLACE VIEW
view_pending_subscriptions(lastname, firstname)
  AS
    SELECT a, b FROM (
      SELECT person.lastname AS a, person.firstname AS b, subscription.status AS c
      FROM person, subscription
      WHERE subscription.status = 'P' AND subscription.person = person.id
      ORDER BY subscription.start_date)
      AS foo;

CREATE OR REPLACE VIEW
view_old_subscription(lastname, firstname, subscription, status)
  AS
    SELECT a, b, c, e FROM (
      SELECT person.lastname AS a,
             person.firstname AS b,
             offer.name AS c,
             subscription.status AS d,
             status.name AS e
      FROM person, offer, subscription, status
      WHERE subscription.person = person.id AND
            offer.code = subscription.offer AND
            subscription.status <> 'R' AND
            subscription.status = status.code AND
            AGE(subscription.start_date) > interval '1 year')
      AS foo
      ORDER BY CONCAT(a, ' ', b), c;

/*---------------------------------------------------------------------------
|---------------------------------------------------------------------------|
|----------------------------TRESHOLD 2-------------------------------------|
|----------------------------Procedures-------------------------------------|
|---------------------------------------------------------------------------|
---------------------------------------------------------------------------*/

CREATE OR REPLACE FUNCTION
list_station_near_user(mail VARCHAR(128))
  RETURNS SETOF VARCHAR(64) AS
$$
  BEGIN
    RETURN QUERY
    SELECT CAST(a AS VARCHAR(64)) FROM (
      SELECT LOWER(station.name) AS a, person.email FROM station, person
      WHERE LOWER(station.town) = LOWER(person.town) AND
            person.email = mail)
      AS foo
    ORDER BY a;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
list_subscribers(code_offer VARCHAR(5))
  RETURNS SETOF VARCHAR(64) AS
$$
  BEGIN
    RETURN QUERY
    SELECT CAST(CONCAT(a, ' ', b) AS VARCHAR(64)) FROM (
      SELECT person.lastname AS a, person.firstname AS b, subscription.offer
      FROM person, subscription
      WHERE subscription.offer = code_offer AND
            subscription.person = person.id)
    AS foo
    ORDER BY CONCAT(a, ' ', b);
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION
list_subscription(_email  VARCHAR(128),
                  _date   DATE)
  RETURNS SETOF VARCHAR(5) AS
$$
  BEGIN
    RETURN QUERY
    SELECT a FROM (
      SELECT offer.code AS a, subscription.offer, person.email
      FROM offer, subscription, person
      WHERE offer.code = subscription.offer AND
            person.id = subscription.person AND
            person.email = _email AND
            subscription.start_date >= _date)
    AS foo
    ORDER BY a;
  END;
$$ LANGUAGE plpgsql;
